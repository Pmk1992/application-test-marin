package model;

public class Marin {

	private String nomM; 
	private double salaireM; 
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomM == null) ? 0 : nomM.hashCode());
		long temp;
		temp = Double.doubleToLongBits(salaireM);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nomM == null) {
			if (other.nomM != null)
				return false;
		} else if (!nomM.equals(other.nomM))
			return false;
		if (Double.doubleToLongBits(salaireM) != Double.doubleToLongBits(other.salaireM))
			return false;
		return true;
	}

	public String getNomM() {
		return nomM;
	}

	public void setNomM(String nomM) {
		this.nomM = nomM;
	}

	public double getSalaireM() {
		return salaireM;
	}

	public void setSalaireM(double salaireM) {
		this.salaireM = salaireM;
	}

	public static void main(String [] args){
		System.out.println("*********************");
		System.out.println("Test with git cloning is working now ;)");
		System.out.println("*********************");
	}
}
